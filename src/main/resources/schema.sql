
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_row;


CREATE TABLE order_row (
   id SERIAL,
   itemName VARCHAR(255) NOT NULL,
   quantity int,
   price int,
   PRIMARY KEY (id)
);
CREATE TABLE orders (
   id SERIAL,
   orderNumber VARCHAR(255) NOT NULL,
   rowId int,
   PRIMARY KEY (id),
   FOREIGN KEY (rowId) REFERENCES order_row(id)
);

