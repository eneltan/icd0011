package servletproject;

import util.DataSourceProvider;
import util.DbUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener()
public class ServletListener implements ServletContextListener {

	public static OrderDao orderDao = new OrderDao();

	DataSourceProvider dataSourceProvider = new DataSourceProvider();

	public void contextInitialized(ServletContextEvent sce) {
		orderDao.createSchema();
		dataSourceProvider.setConnectionInfo(orderDao.getConnectionInfo());
		dataSourceProvider.getDataSource();
	}

	public void contextDestroyed(ServletContextEvent sce) {
		DataSourceProvider.closePool();
	}
}
