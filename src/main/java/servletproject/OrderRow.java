package servletproject;

public class OrderRow {
	private Long rowId;
	private String itemName;
	private Integer quantity;
	private Integer price;
	private static Long count = 0L;

	@Override
	public String toString() {
		return "OrderRow{" + "rowId='" + rowId + '\'' + ", itemName='" + itemName + '\'' + ", quantity=" + quantity
				+ ", price=" + price + '}';
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Long getRowId() {
		return rowId;
	}

	public void setRowId() {
		this.rowId = count;
	}
}