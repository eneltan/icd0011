package servletproject;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
//import util.DbUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/api/orders")
public class OrderServlet extends HttpServlet {

	private static OrderService service = new OrderService();

//	private static OrderDao orderDao = new OrderDao(DbUtil.loadConnectionInfo());

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setHeader("Content-Type", "application/json");

		String input = IOUtils.toString(req.getInputStream(), "UTF-8");

		Order order = new ObjectMapper().readValue(input, Order.class);
		String json = new ObjectMapper().writeValueAsString(service.saveOrder(order));

		resp.getWriter().print(json);

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setHeader("Content-Type", "application/json");

		String id = req.getParameter("id");
		if (id != null){
			Order order = service.getOrder(id);
			String json = new ObjectMapper().writeValueAsString(order);
			resp.getWriter().print(json);
		}
		else {
			List order = service.getAllOrdes();
			String json = new ObjectMapper().writeValueAsString(order);
			resp.getWriter().print(json);
		}
	}
}
