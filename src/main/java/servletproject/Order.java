package servletproject;

import java.io.Serializable;
import java.util.List;

public class Order implements Serializable {
	private Long id;
	private String orderNumber;
	private List<OrderRow> orderRows;

	public Order() {

	}

	public Order(long id, String orderNumber) {
		this.id = id;
		this.orderNumber = orderNumber;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<OrderRow> getOrderRows() {
		return orderRows;
	}

	public void setOrderRows(List<OrderRow> orderRow) {
		for (OrderRow i : orderRow) {
			i.setRowId();
		}
		this.orderRows = orderRow;
	}
}
