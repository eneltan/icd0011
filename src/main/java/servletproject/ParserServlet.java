package servletproject;


import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("api/parser")
public class ParserServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String input = IOUtils.toString(req.getInputStream());

		List<String> stringList = parser(input);
		resp.setHeader("Content-Type", "application/json");
		resp.getWriter().print(stringer(stringList));
	}

	public List<String> parser(String input){
		List<String> stringList =  new ArrayList<>();
		input = input.replace("}","");
		input = input.replace("{","");
		input = input.replace("\"","").trim();
		String[] splittedByComma = input.split(",");

		for (String i : splittedByComma) {
			String[] splittedByColon = i.split(":");

			String f = splittedByColon[0].replace("\"", "").trim();
			String h = splittedByColon[1].replace("\"", "").trim();

			stringList.add(f);
			stringList.add(h);
		}
		return stringList;
	}

	public String stringer(List stringList){
		StringBuilder result = new StringBuilder();
		result.append("{");
		for (int i = 0; i < stringList.size(); i++) {
			StringBuilder str = new StringBuilder();
			str.append("\"").append(stringList.get(i)).append("\"");
			str.reverse();
			if (i % 2 == 0) {
				str.append(":");
			} else if ( i + 1 != stringList.size()) {
				str.append(",");
			}
			result.append(str.toString());
		}
		result.append("}");
		return result.toString();
	}
}
