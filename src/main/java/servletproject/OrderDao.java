package servletproject;

import util.ConnectionInfo;
import util.DbUtil;
import util.FileUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrderDao {



	public static ConnectionInfo getConnectionInfo() {
		return connectionInfo;
	}

	public static void setConnectionInfo(ConnectionInfo connectionInfo) {
		OrderDao.connectionInfo = connectionInfo;
	}

	private static ConnectionInfo connectionInfo;

	public OrderDao() {
		this.connectionInfo = DbUtil.loadConnectionInfo();
	}


	public OrderDao(ConnectionInfo connectionInfo) { this.connectionInfo = connectionInfo; }

	public static Order saveOrder(Order order) {
		try
		{
			Connection conn = DriverManager.getConnection(
					connectionInfo.getUrl(),
					connectionInfo.getUser(),
					connectionInfo.getPass()); Statement stmt = conn.createStatement();

			stmt.executeUpdate("INSERT INTO orders (orderNumber)" + "VALUES ('" + order.getOrderNumber()+ "')");
			return getOrderByNumber(order.getOrderNumber());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
//		return order;
	}

	public static List<Order> findOrders() {
		try
			{
				Connection conn = DriverManager.getConnection(
						connectionInfo.getUrl(),
						connectionInfo.getUser(),
						connectionInfo.getPass()); Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("select id, orderNumber from orders");

			List<Order> orders = new ArrayList<>();

			while (rs.next()) {
				//Order order = new Order(rs.getInt("id"), rs.getString("orderNumber"));
				Order order = new Order(rs.getInt("id"), rs.getString("orderNumber"));
				orders.add(order);
			}
			return orders;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	private static Order getOrderByNumber(String id) {
		try
		{
			Connection conn = DriverManager.getConnection(
					connectionInfo.getUrl(),
					connectionInfo.getUser(),
					connectionInfo.getPass());
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("select id, orderNumber from orders where ordernumber = '"+ id + "'" );

			rs.next();
			Order order = new Order();
			order.setId(rs.getLong("id"));
			order.setOrderNumber(rs.getString("orderNumber"));
//			rs.getObject()
			return order;

//			return rs.getObject();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public static Order getOrder(String id) {
		try
		{
			Connection conn = DriverManager.getConnection(
					connectionInfo.getUrl(),
					connectionInfo.getUser(),
					connectionInfo.getPass());
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("select id, orderNumber from orders where id = '"+ id + "'" );

			rs.next();
			Order order = new Order();
			order.setId(rs.getLong("id"));
			order.setOrderNumber(rs.getString("orderNumber"));
//			rs.getObject()
			return order;

//			return rs.getObject();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public static void createSchema() {


		try (Connection conn = DriverManager.getConnection(
				connectionInfo.getUrl(),
				connectionInfo.getUser(),
				connectionInfo.getPass()); Statement stmt = conn.createStatement()) {

			String sql = FileUtil.readFileFromClasspath("schema.sql");

			stmt.executeUpdate(sql);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}
}
