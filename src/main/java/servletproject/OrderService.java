package servletproject;

import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

public class OrderService {

//	private static Map<Long, Order> orderMap = new HashMap<>();


	public Order saveOrder(Order order) throws IOException {
		OrderDao orderDao = new OrderDao();
		Order newOrder = orderDao.saveOrder(order);
		return newOrder;
	}

	public Order getOrder(String id){
		OrderDao orderDao = new OrderDao();
			return orderDao.getOrder(id);
	}
	public List getAllOrdes () {
		OrderDao orderDao = new OrderDao();
		return orderDao.findOrders();
	}
}
